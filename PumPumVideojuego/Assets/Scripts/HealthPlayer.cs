using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class HealthPlayer : MonoBehaviour
{
    public PlayerController player;
    // Start is called before the first frame update
    void Start()
    {
        ChangeText();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeText()
    {
        string hp = player.HealthPoints.ToString();
        this.GetComponent<TextMeshProUGUI>().text = "HP: " + hp;
    }
}
