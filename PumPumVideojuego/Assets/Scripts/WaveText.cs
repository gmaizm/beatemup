using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class WaveText : MonoBehaviour
{
    public Waves waveInfo;
    // Start is called before the first frame update
    void Start()
    {
        ChangeText();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ChangeText()
    {
        string w = waveInfo.numWave.ToString();
        this.GetComponent<TextMeshProUGUI>().text = "Wave: " + w;
    }
}
