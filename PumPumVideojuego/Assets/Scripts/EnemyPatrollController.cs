using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPatrollController : MonoBehaviour
{
    private enum SwitchMachineStates { NONE, IDLE, CHASE, ATTACK, PATROLL, DAMAGED};
    private SwitchMachineStates CurrentState;
    public Vector3[] waypoints = new Vector3[2];
    private int target = 0;
    private bool returnpatroll = false;
    public Waves waveinfo;

    private void ChangeState(SwitchMachineStates newState)
    {
        if (newState == CurrentState)
            return;

        ExitState();
        InitState(newState);
    }
    private void InitState(SwitchMachineStates currentState)
    {
        CurrentState = currentState;
        switch (CurrentState)
        {
            case SwitchMachineStates.IDLE:

                Rigidbody.velocity = Vector2.zero;
                Debug.Log("Me paro");
                Animator.Play("idleEnemy");

                break;

            case SwitchMachineStates.CHASE:

                Debug.Log("Chase es el mejor opening de jojos");
                Animator.Play("chase");

                break;

            case SwitchMachineStates.ATTACK:

                StartCoroutine(shoot());

                break;

            case SwitchMachineStates.PATROLL:

                Debug.Log("Patrullando mi pana");
                Animator.Play("patroll");

                break;
            case SwitchMachineStates.DAMAGED:
                Animator.Play("damaged");

                break;

            default:
                break;
        }
    }
    private void ExitState()
    {
        switch (CurrentState)
        {
            case SwitchMachineStates.IDLE:
                StopAllCoroutines();
                break;

            case SwitchMachineStates.CHASE:
                break;

            case SwitchMachineStates.ATTACK:
                StopAllCoroutines();
                break;
            case SwitchMachineStates.PATROLL:
                break;
            case SwitchMachineStates.DAMAGED:

                break;

            default:
                break;
        }
    }

    private void UpdateState()
    {
        switch (CurrentState)
        {
            case SwitchMachineStates.IDLE:

                if (this.gameObject.GetComponentInChildren<Detection>().detecta)
                    ChangeState(SwitchMachineStates.CHASE);
                if (returnpatroll)
                {
                    returnpatroll = false;
                    StartCoroutine(confuse());
                }

                break;
            case SwitchMachineStates.CHASE:
                Rigidbody.velocity = (tjugador - transform.position).normalized * Speed;

                if (Rigidbody.velocity.x < 0 && transform.eulerAngles.y != 180)
                    transform.eulerAngles = new Vector3(0, 180, 0);
                if (Rigidbody.velocity.x > 0 && transform.eulerAngles.y != 0)
                    transform.eulerAngles = Vector3.zero;

                if (!this.gameObject.GetComponentInChildren<Detection>().detecta)
                    ChangeState(SwitchMachineStates.IDLE);
                returnpatroll = true;

                if (this.gameObject.GetComponentInChildren<AttackDetection>().detectaattack)
                    ChangeState(SwitchMachineStates.ATTACK);

                break;
            case SwitchMachineStates.ATTACK:
                if (!this.gameObject.GetComponentInChildren<AttackDetection>().detectaattack)
                    ChangeState(SwitchMachineStates.CHASE);

                break;

            case SwitchMachineStates.PATROLL:
                Rigidbody.velocity = (waypoints[target] - transform.position).normalized * Speed;

                if (Rigidbody.velocity.x < 0 && transform.eulerAngles.y != 180)
                    transform.eulerAngles = new Vector3(0, 180, 0);
                if (Rigidbody.velocity.x > 0 && transform.eulerAngles.y != 0)
                    transform.eulerAngles = Vector3.zero;


                if (Vector3.SqrMagnitude(waypoints[target] - this.transform.position) <= m_SQDistancePerFrame)
                    if (target == 0) target = 1;
                    else target = 0;

                if (this.gameObject.GetComponentInChildren<Detection>().detecta)
                    ChangeState(SwitchMachineStates.CHASE);
                break;
            case SwitchMachineStates.DAMAGED:
                if (this.HealthPoints <= 0)
                {
                    this.gameObject.SetActive(false);
                    waveinfo.enemiesLeft--;
                } else
                {
                    ChangeState(SwitchMachineStates.IDLE);
                }
                
                break;

            default:
                break;
        }
    }
    IEnumerator confuse()
    {
        yield return new WaitForSeconds(1);
        ChangeState(SwitchMachineStates.PATROLL);

    }

    IEnumerator shoot()
    {
        while (true)
        {
            yield return new WaitForSeconds(3);
            GameObject novabala = BalaPool.instance.GetPooledBala();
            novabala.SetActive(true);
            novabala.GetComponent<BulletController>().Damage = this.DamageBala;
            novabala.GetComponent<BulletController>().Speed = this.SpeedBala;
            novabala.GetComponent<Transform>().position = transform.position;
            novabala.GetComponent<Rigidbody2D>().velocity = (tjugador - transform.position).normalized * novabala.GetComponent<BulletController>().Speed;
            
        }

    }

    private Animator Animator;
    private Rigidbody2D Rigidbody;

    private float m_SQDistancePerFrame;
    private Vector3 tjugador;
    public PlayerInfo playerInfo;

    [Header("Enemy Values")]
    [SerializeField]
    private float Speed = 2;
    [SerializeField]
    private int HealthPoints = 10;
    [SerializeField]
    private int DamageBala;
    [SerializeField]
    private float SpeedBala;


    void Awake()
    {
        Rigidbody = GetComponent<Rigidbody2D>();
        tjugador = playerInfo.playerpos;
        Animator = GetComponent<Animator>();
        m_SQDistancePerFrame = Speed * Time.fixedDeltaTime;
        m_SQDistancePerFrame *= m_SQDistancePerFrame;
    }

    private void Start()
    {
        InitState(SwitchMachineStates.PATROLL);
    }

    void Update()
    {
        tjugador = playerInfo.playerpos;
        UpdateState();
    }

    private void OnDestroy()
    {
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.name == "Hitbox")
        {
            this.HealthPoints -= collision.gameObject.GetComponent<HitboxInfo>().Damage;
            Debug.Log(this.HealthPoints);
            ChangeState(SwitchMachineStates.DAMAGED);

        }
    }
    public void inicialitzaSO(EnemyRanged enemy)
    {
        this.Speed = enemy.Speed;
        this.HealthPoints = enemy.HP;
        this.DamageBala = enemy.Damage;
        this.SpeedBala = enemy.shootingSpeed;

    }
    public void inicialitzaSO(EnemyMelee enemy)
    {
        gameObject.GetComponentInChildren<HitboxInfo>().Damage = enemy.Damage;
        this.Speed = enemy.Speed;
        this.HealthPoints = enemy.HP;
        InitState(SwitchMachineStates.PATROLL);
    }
}
