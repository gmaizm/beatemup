using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMeleePool : MonoBehaviour
{
    public static EnemyMeleePool instance;
    private List<GameObject> pooledEnemyMelee = new List<GameObject>();
    private int amountPool = 20;

    [SerializeField] private GameObject enemymelee;

    private void Awake()
    {
        {
            if (instance == null)
            {
                if (instance == null)
                {
                    instance = this;
                }
            }
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < amountPool; i++)
        {
            GameObject obj = Instantiate(enemymelee);
            obj.SetActive(false);
            pooledEnemyMelee.Add(obj);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public GameObject GetPooledEnemyMelee()
    {
        for (int i = 0; i < pooledEnemyMelee.Count; i++)
        {
            if (!pooledEnemyMelee[i].activeInHierarchy)
            {
                return pooledEnemyMelee[i];
            }
        }
        return null;
    }
}
