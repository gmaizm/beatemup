using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRangedPool : MonoBehaviour
{
    public static EnemyRangedPool instance;
    private List<GameObject> pooledEnemyRanged = new List<GameObject>();
    private int amountPool = 20;

    [SerializeField] private GameObject enemyranged;

    private void Awake()
    {
        {
            if (instance == null)
            {
                if (instance == null)
                {
                    instance = this;
                }
            }
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < amountPool; i++)
        {
            GameObject obj = Instantiate(enemyranged);
            obj.SetActive(false);
            pooledEnemyRanged.Add(obj);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public GameObject GetPooledEnemyRanged()
    {
        for (int i = 0; i < pooledEnemyRanged.Count; i++)
        {
            if (!pooledEnemyRanged[i].activeInHierarchy)
            {
                return pooledEnemyRanged[i];
            }
        }
        return null;
    }
}
