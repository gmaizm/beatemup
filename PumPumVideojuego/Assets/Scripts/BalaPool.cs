using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalaPool : MonoBehaviour
{
    public static BalaPool instance;
    private List<GameObject> pooledBala = new List<GameObject>();
    private int amountPool = 20;

    [SerializeField] private GameObject bala;

    private void Awake()
    {
        {
            if (instance == null)
            {
                if (instance == null)
                {
                    instance = this;
                }
            }
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < amountPool; i++)
        {
            GameObject obj = Instantiate(bala);
            obj.SetActive(false);
            pooledBala.Add(obj);
        }
    }

    public GameObject GetPooledBala()
    {
        for (int i = 0; i < pooledBala.Count; i++)
        {
            if (!pooledBala[i].activeInHierarchy)
            {
                return pooledBala[i];
            }
        }
        return null;
    }

}
