using System.Collections;
using System.Collections.Generic;
using UnityEditor.Timeline.Actions;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    private enum SwitchMachineStates { NONE, IDLE, WALK, WEAK, STRONG, DAMAGED};
    private SwitchMachineStates CurrentState;
    public GameEvent playerdamaged;

    private void ChangeState(SwitchMachineStates newState)
    {
        if (newState == CurrentState)
            return;

        ExitState();
        InitState(newState);
    }
    private void InitState(SwitchMachineStates currentState)
    {
        CurrentState = currentState;
        switch (CurrentState)
        {
            case SwitchMachineStates.IDLE:

                Rigidbody.velocity = Vector2.zero;
                Animator.Play("idle");


                break;

            case SwitchMachineStates.WALK:

                Animator.Play("walk");

                break;

            case SwitchMachineStates.WEAK:

                ComboAvilable = false;
                Rigidbody.velocity = Vector2.zero;
                HitboxInfo.Damage = WeakDamage;
                Animator.Play("weak");

                break;

            case SwitchMachineStates.STRONG:

                ComboAvilable = false;
                Rigidbody.velocity = Vector2.zero;
                HitboxInfo.Damage = StrongDamage;
                Animator.Play("strong");

                break;

            case SwitchMachineStates.DAMAGED:
                if (HealthPoints <= 0)
                {
                    SceneManager.LoadScene("GameOver");
                }
                Animator.Play("damaged");
                playerdamaged.Raise();
                break;

            default:
                break;
        }
    }
    private void ExitState()
    {
        switch (CurrentState)
        {
            case SwitchMachineStates.IDLE:

                break;

            case SwitchMachineStates.WALK:

                break;

            case SwitchMachineStates.WEAK:

                ComboAvilable = false;

                break;

            case SwitchMachineStates.STRONG:

                ComboAvilable = false;

                break;

            case SwitchMachineStates.DAMAGED:
                
                break;

            default:
                break;
        }
    }

    private void UpdateState()
    {
        switch (CurrentState)
        {
            case SwitchMachineStates.IDLE:

                if (MovementAction.ReadValue<Vector2>().x != 0)
                    ChangeState(SwitchMachineStates.WALK);

                break;
            case SwitchMachineStates.WALK:

                Rigidbody.velocity = new Vector2(MovementAction.ReadValue<Vector2>().x * Speed, Rigidbody.velocity.y);

                if (Rigidbody.velocity.x < 0 && transform.eulerAngles.y != 180)
                    transform.eulerAngles = new Vector3(0, 180, 0);
                if (Rigidbody.velocity.x > 0 && transform.eulerAngles.y != 0)
                    transform.eulerAngles = Vector3.zero;

                if (Rigidbody.velocity == Vector2.zero)
                    ChangeState(SwitchMachineStates.IDLE);

                break;
            case SwitchMachineStates.WEAK:

                break;

            case SwitchMachineStates.STRONG:

                break;

            case SwitchMachineStates.DAMAGED:
                ChangeState(SwitchMachineStates.IDLE);
                break;


            default:
                break;
        }
    }

    private bool ComboAvilable = false;

    public void InitComboWindow()
    {
        ComboAvilable = true;
    }

    public void EndComboWindow()
    {
        ComboAvilable = false;
        WeakDamage = 2;
        StrongDamage = 5;
    }

    public void EndHit()
    {
        ChangeState(SwitchMachineStates.IDLE);
    }
    private void WeakAttackAction(InputAction.CallbackContext actionContext)
    {
        switch (CurrentState)
        {
            case SwitchMachineStates.IDLE:
                ChangeState(SwitchMachineStates.WEAK);

                break;

            case SwitchMachineStates.WALK:
                ChangeState(SwitchMachineStates.WEAK);

                break;

            case SwitchMachineStates.WEAK:

                EndComboWindow();

                break;

            case SwitchMachineStates.STRONG:
                if (ComboAvilable)
                    WeakDamage *= 2;
                    print(WeakDamage);
                    ChangeState(SwitchMachineStates.STRONG);

                break;

            case SwitchMachineStates.DAMAGED:

                break;

            default:
                break;
        }
    }

    private void StrongAttackAction(InputAction.CallbackContext actionContext)
    {
        switch (CurrentState)
        {
            case SwitchMachineStates.IDLE:
                ChangeState(SwitchMachineStates.STRONG);

                break;

            case SwitchMachineStates.WALK:
                ChangeState(SwitchMachineStates.STRONG);

                break;

            case SwitchMachineStates.WEAK:
                if (ComboAvilable)
                    StrongDamage *= 2;
                    print(StrongDamage);
                    ChangeState(SwitchMachineStates.WEAK);

                break;

            case SwitchMachineStates.STRONG:
                EndComboWindow();
                break;

            case SwitchMachineStates.DAMAGED:

                break;

            default:
                break;
        }
    }




    [SerializeField]
    private InputActionAsset InputAsset;
    private InputActionAsset Input;
    private InputAction MovementAction;
    private Animator Animator;
    private Rigidbody2D Rigidbody;
    private SpriteRenderer SpriteRenderer;
    [SerializeField]
    private HitboxInfo HitboxInfo;

    [Header("Character Values")]
    [SerializeField]
    private float Speed = 2;
    [SerializeField]
    private int WeakDamage = 2;
    [SerializeField]
    private int StrongDamage = 5;
    [SerializeField]
    public int HealthPoints;

    public PlayerInfo PlayerInfo;

    void Awake()
    {
        Assert.IsNotNull(InputAsset);

        Input = Instantiate(InputAsset);
        MovementAction = Input.FindActionMap("Standard").FindAction("Movement");
        Input.FindActionMap("Standard").FindAction("WeakAttack").performed += WeakAttackAction;
        Input.FindActionMap("Standard").FindAction("StrongAttack").performed += StrongAttackAction;
        Input.FindActionMap("Standard").Enable();

        Rigidbody = GetComponent<Rigidbody2D>();
        Animator = GetComponent<Animator>();
        SpriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Start()
    {
        InitState(SwitchMachineStates.IDLE);
    }

    void Update()
    {
        PlayerInfo.playerpos = this.gameObject.GetComponent<Transform>().position;
        UpdateState();
    } 

    private void OnDestroy()
    {
        Input.FindActionMap("Standard").FindAction("WeakAttack").performed -= WeakAttackAction;
        Input.FindActionMap("Standard").FindAction("StrongAttack").performed -= StrongAttackAction;
        Input.FindActionMap("Standard").Disable();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.name == "EnemyHitbox")
        { 
            this.HealthPoints -= collision.gameObject.GetComponent<HitboxInfo>().Damage;
            Debug.Log(collision.gameObject.GetComponent<HitboxInfo>().Damage);
            Debug.Log(this.HealthPoints);
            ChangeState(SwitchMachineStates.DAMAGED);

        }

        if (collision.gameObject.tag == "Bullet")
        {
            this.HealthPoints -= collision.gameObject.GetComponent<BulletController>().Damage;
            Debug.Log(this.HealthPoints);
            collision.gameObject.SetActive(false);
            ChangeState(SwitchMachineStates.DAMAGED);

        }


    }
}
