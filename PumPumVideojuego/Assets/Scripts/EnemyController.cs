using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.InputSystem;
using static UnityEngine.GraphicsBuffer;

public class EnemyController : MonoBehaviour
{
    private enum SwitchMachineStates { NONE, IDLE, CHASE, ATTACK, DAMAGED};
    private SwitchMachineStates CurrentState;
    public Waves waveinfo;

    private void ChangeState(SwitchMachineStates newState)
    {
        if (newState == CurrentState)
            return;

        ExitState();
        InitState(newState);
    }
    private void InitState(SwitchMachineStates currentState)
    {
        CurrentState = currentState;
        switch (CurrentState)
        {
            case SwitchMachineStates.IDLE:

                Rigidbody.velocity = Vector2.zero;
                Debug.Log("Me paro");
                Animator.Play("idleEnemy");

                break;

            case SwitchMachineStates.CHASE:

                Debug.Log("Chase es el mejor opening de jojos");
                Animator.Play("chase");

                break;

            case SwitchMachineStates.ATTACK:

                Animator.Play("attack");

                break;
            case SwitchMachineStates.DAMAGED:
                Animator.Play("damaged");

                break;

            default:
                break;
        }
    }
    private void ExitState()
    {
        switch (CurrentState)
        {
            case SwitchMachineStates.IDLE:

                break;

            case SwitchMachineStates.CHASE:
                break;

            case SwitchMachineStates.ATTACK:
                StopAllCoroutines();
                break;
            case SwitchMachineStates.DAMAGED:

                break;

            default:
                break;
        }
    }

    private void UpdateState()
    {
        switch (CurrentState)
        {
            case SwitchMachineStates.IDLE:

                if (this.gameObject.GetComponentInChildren<Detection>().detecta)
                    ChangeState(SwitchMachineStates.CHASE);
                
                break;
            case SwitchMachineStates.CHASE:
                Rigidbody.velocity = (tjugador - transform.position).normalized*Speed;

                if (Rigidbody.velocity.x < 0 && transform.eulerAngles.y != 180)
                    transform.eulerAngles = new Vector3(0, 180, 0);
                if (Rigidbody.velocity.x > 0 && transform.eulerAngles.y != 0)
                    transform.eulerAngles = Vector3.zero;

                if (!this.gameObject.GetComponentInChildren<Detection>().detecta)
                    ChangeState(SwitchMachineStates.IDLE);

                if (this.gameObject.GetComponentInChildren<AttackDetection>().detectaattack)
                    ChangeState(SwitchMachineStates.ATTACK);

                break;
            case SwitchMachineStates.ATTACK:
                if (!this.gameObject.GetComponentInChildren<AttackDetection>().detectaattack)
                    ChangeState(SwitchMachineStates.CHASE);

                break;
            case SwitchMachineStates.DAMAGED:
                if (this.HealthPoints <= 0)
                {
                    this.gameObject.SetActive(false);
                    waveinfo.enemiesLeft--;
                }
                else
                {
                    ChangeState(SwitchMachineStates.IDLE);
                }

                break;

            default:
                break;
        }
    }

    private Animator Animator;
    private Rigidbody2D Rigidbody;
    [SerializeField]

    private Vector3 tjugador;

    public PlayerInfo playerInfo;

    [Header("Enemy Values")]
    [SerializeField]
    private float Speed = 2;
    [SerializeField]
    private int HealthPoints = 10;

    void Awake()
    {
        Rigidbody = GetComponent<Rigidbody2D>();
        tjugador = playerInfo.playerpos;
        Animator = GetComponent<Animator>();
    }

    private void Start()
    {
        InitState(SwitchMachineStates.IDLE);
    }

    void Update()
    {
        tjugador = playerInfo.playerpos;
        UpdateState();
    }

    private void OnDestroy()
    {
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.name == "Hitbox")
        {
            this.HealthPoints -= collision.gameObject.GetComponent<HitboxInfo>().Damage;
            Debug.Log(this.HealthPoints);
            ChangeState(SwitchMachineStates.DAMAGED);

        }
    }

    public void inicialitzaSO(EnemyMelee enemy)
    {
        gameObject.GetComponentInChildren<HitboxInfo>().Damage = enemy.Damage;
        this.Speed = enemy.Speed;
        this.HealthPoints = enemy.HP;
    }
}
