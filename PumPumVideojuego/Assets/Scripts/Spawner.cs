using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public List<EnemyMelee> meleeList;
    public List<EnemyRanged> rangedList;
    public Waves waveinfo;
    public GameEvent ChangeWave;

    // Start is called before the first frame update
    void Start()
    {
        waveinfo.numWave = 0;
        waveinfo.enemiesLeft = 0;
        waveinfo.numenemies = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (waveinfo.enemiesLeft == 0)
        {
            setWave();
            ChangeWave.Raise();
        }
    }

    public void setWave()
    {
        ++waveinfo.numWave;
        
        if (waveinfo.numWave == 1) 
        {
            spawnEnemyMelee(meleeList[0]);
            
            ++waveinfo.numenemies;
            waveinfo.enemiesLeft = waveinfo.numenemies;



        } else if (waveinfo.numWave == 2)
        {
            spawnEnemyRanged(rangedList[0]);
            waveinfo.enemiesLeft = waveinfo.numenemies;
        } else if (waveinfo.numWave > 2 && waveinfo.numWave <= 4) 
        {
            ++waveinfo.numenemies;
            waveinfo.enemiesLeft = waveinfo.numenemies;
            for (int i = 0; i < waveinfo.numenemies; ++i)
            {
                int r0 = Random.Range(1, 3);
                if (r0 == 1)
                {
                    spawnEnemyMelee(meleeList[0]);
                }
                else
                {
                    spawnEnemyRanged(rangedList[0]);
                }
            }
            
            
        } else if (waveinfo.numWave > 4 && waveinfo.numWave <= 6)
        {
            ++waveinfo.numenemies;
            waveinfo.enemiesLeft = waveinfo.numenemies;
            for (int i = 0; i < waveinfo.numenemies; ++i)
            {
                int r0 = Random.Range(1, 3);
                if (r0 == 1)
                {
                    spawnEnemyMelee(meleeList[1]);
                }
                else
                {
                    spawnEnemyRanged(rangedList[1]);
                }
            }


        }else 
        {
            ++waveinfo.numenemies;
            waveinfo.enemiesLeft = waveinfo.numenemies;
            for (int i = 0; i < waveinfo.numenemies; ++i)
            {
                int r0 = Random.Range(1, 3);
                if (r0 == 1)
                {
                    spawnEnemyMelee(meleeList[2]);
                }
                else
                {
                    spawnEnemyRanged(rangedList[2]);
                }
            }


        }
    }

    public void spawnEnemyMelee(EnemyMelee enemyLvL)
    {
        GameObject newenemy = EnemyMeleePool.instance.GetPooledEnemyMelee();
        if (newenemy != null)
        {
            newenemy.SetActive(true);
            newenemy.GetComponent<EnemyController>().inicialitzaSO(enemyLvL);
            int r = Random.Range(1, 3);
            bool izq = false;
            if (r == 1) izq = true;
            if (izq)
            {
                newenemy.GetComponent<Transform>().position = new Vector2(Random.Range(-8.5f, -1.5f), -2);
            }
            else
            {
                newenemy.GetComponent<Transform>().position = new Vector2(Random.Range(1.5f, 8.5f), -2);
            }


        }
    }
    public void spawnEnemyRanged(EnemyRanged enemyRangedLvL)
    {
        GameObject newenemy = EnemyRangedPool.instance.GetPooledEnemyRanged();
        if (newenemy != null)
        {
            newenemy.SetActive(true);
            newenemy.GetComponent<EnemyPatrollController>().inicialitzaSO(enemyRangedLvL);
            int r = Random.Range(1, 3);
            bool izq = false;
            if (r == 1) izq = true;
            if (izq)
            {
                float rpos = Random.Range(-8.5f, -1.5f);
                newenemy.GetComponent<Transform>().position = new Vector2(rpos, -2);
                newenemy.GetComponent<EnemyPatrollController>().waypoints[0] = new Vector3(rpos - 1, -2, 0);
                newenemy.GetComponent<EnemyPatrollController>().waypoints[1] = new Vector3(rpos + 1, -2, 0);
            }
            else
            {
                float rpos = Random.Range(1.5f, 8.5f);
                newenemy.GetComponent<Transform>().position = new Vector2(rpos, -2);
                newenemy.GetComponent<EnemyPatrollController>().waypoints[0] = new Vector3(rpos - 1, -2, 0);
                newenemy.GetComponent<EnemyPatrollController>().waypoints[1] = new Vector3(rpos + 1, -2, 0);
            }

        }
    }
}
