using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class EnemyMelee : ScriptableObject
{
    public int Damage;
    public float Speed;
    public int HP;
}
