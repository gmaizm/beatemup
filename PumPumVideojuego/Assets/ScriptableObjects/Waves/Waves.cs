using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Waves : ScriptableObject
{
    public int numWave;
    public int enemiesLeft;
    public int numenemies;
}
